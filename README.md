# Leafony & XIAO 連携ボード

Leafony と Seeed株式会社から販売されている XIAOシリーズのマイコンを 連携するための基板を自作しました。

100milピッチ７ピンｘ２列のピンヘッダーから Leafonyバスに接続する変換基板とも言えます。
Arafruit社が販売する QT Py シリーズも、XIAOと同じコネクタ形状ですので、組み合わせの幅が広がると思います。

## Leafony & XIAO 連携ボード 概略仕様

- Leafonyバスと、XIAOを実装する７ｘ２列のランド
- Seeed社の”GROVE" コネクタ。アナログ入力、デジタル入出力、UARTで使用できます
- SparkFun社の ”QUIIC" コネクタ。 I2C信号と3.3V電源の４ピンです
- ６ピンのコネクタ。Autopilot機器のテレメトリーポートに繋ぐ予定です

![Leafony-XIAO_Board](/images/Leafony_XIAO_Board.png)

## XIAOとLeafonyバスの接続

![Leafony-XIAO_信号割当て](/images/Leafony-XIAO-Board_Signal-assignment.png)

## 回路図

![Leafony-XIAO_基板回路図](/images/Leafony-XIAO-board_Schematics.png)
